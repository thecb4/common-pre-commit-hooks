#!/usr/bin/env bash

COMMIT_MODIFIED=$(git status --short | grep COMMIT.md)
[ "$COMMIT_MODIFIED" ] || exit 1